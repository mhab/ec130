This project is about the "Airbus Helicopter EC130 B4 / H130" light helicopter models for the OpenSource flight-simulator "Flightgear".

Find more info in the gitlab Wiki: https://gitlab.com/mhab/ec130/wikis/home

Find detailed info about this project in Flightgear Wiki: http://wiki.flightgear.org/H130

In order to use this aircraft model you need to install Flightgear flight simulator. Flightgear Main Web Site: http://www.flightgear.org/
